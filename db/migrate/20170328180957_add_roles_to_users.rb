class AddRolesToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :users,:unique_id,:string
  	add_column :users,:is_admin,:boolean, :default => false
  	add_column :users,:is_approved,:boolean, :default => false
  	add_column :users,:phone_number,:string
  	add_column :users,:address ,:text
  	add_column :users,:acc_no ,:string
  	add_column :users,:ifsc ,:string
  	add_column :users,:country ,:string
  	add_column :users,:state,:string
  	add_column :users,:city ,:string
  	add_column :users,:pan ,:string
  	add_column :users,:pan_data ,:string
  	add_column :users,:address_data ,:string
  end
end
