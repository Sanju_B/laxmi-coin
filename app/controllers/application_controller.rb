class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!, except: [:home, :about, :contact]

  def home
      
     @approved_users = User.where(:is_approved => true ,:is_admin => false)
     @total_users = User.where(:is_admin => false)
    begin
     w_add = BlockIo.get_new_address(:label => current_user.unique_id.to_s)  
    rescue Exception=>e
           @w_address =  BlockIo.get_address_by_label(:label => current_user.unique_id.to_s) if current_user.present?
      ensure
           @w_address =  BlockIo.get_address_by_label(:label => current_user.unique_id.to_s) if current_user.present?

    end
  end

  def about

  end

  def contact
  end

  def secret
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name,:photo,:pan,:ifsc,:address_data,:pan_data,:phone_number,:acc_no,:country,:state,:city,:unique_id])
  end
end
