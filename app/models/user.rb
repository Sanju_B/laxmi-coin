class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable #,:confirmable
  mount_uploader :photo,AvatarUploader
  mount_uploader :pan_data,AvatarUploader
  mount_uploader :address_data, AvatarUploader
  after_create :generate_unique_id

  def generate_unique_id
  	
  	self.unique_id = Time.now.strftime("%d%m%y") + self.id.to_s  
        begin
          w_add = BlockIo.get_new_address(:label => self.unique_id.to_s)  
    rescue Exception=>e
           w_address =  BlockIo.get_address_by_label(:label => self.unique_id.to_s) 
      ensure
           w_address =  BlockIo.get_address_by_label(:label => self.unique_id.to_s) 

    end
  	self.save!
  end
   
end
